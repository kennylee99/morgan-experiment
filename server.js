const express = require('express');
const morgan = require('morgan'); // import morgan
const rfs = require("rotating-file-stream");


const app = express();

// MORGAN SETUP
// create a log stream
const rfsStream = rfs.createStream(process.env.LOG_FILE || 'log.txt', {
    size: '10M', // rotate every 10 MegaBytes written
    interval: '2m', // rotate after 2 minutes
    compress: 'gzip' // compress rotated files
 });
 
 // if log file defined then use rfs stream else print to console
 app.use(morgan(process.env.LOG_FORMAT || "dev", {
    stream: "log.txt" ? rfsStream : process.stdout
 }));
 
 // if log file is defined then also show logs in console
 // else it will use the previous process.stdout to print to console
 if(process.env.LOG_FILE) {
    app.use(morgan(process.env.LOG_FORMAT || "dev"));    
 }


app.get("/api/v1/log1", (req, res) => {
    res.json({data: "Test logging #1."})
})
app.get("/api/v1/log2", (req, res) => {
    res.json({data: "Test logging #2."})
})
app.get("/api/v1/log3", (req, res) => {
    res.json({data: "Test logging #3."})
})

app.listen(3000, () => {
    console.debug('App listening on :3000');
});